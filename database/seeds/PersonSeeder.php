<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class PersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('people')->insert(
            [
                '_id' => Uuid::uuid4(),
                'name' => "Uhuru Kenyatta",
                'description' => "President of Kenya",
                'image_url' => "https://res.cloudinary.com/voltus/image/upload/v1590354547/voltus/Kenya/Uhuru_Kenyatta_president-kenya-2015-wikimedia-commons_owx8s8.jpg"
            ]
        );

        DB::table('people')->insert(
            [
                '_id' => Uuid::uuid4(),
                'name' => "William Ruto",
                'description' => "Deputy President of Kenya",
                'image_url' => "https://res.cloudinary.com/voltus/image/upload/v1590356602/voltus/Kenya/external-content.duckduckgo.com_nxxjae.jpg"
            ]
        );
    }
}

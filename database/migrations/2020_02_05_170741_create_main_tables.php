<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('_id')->unique();
            $table->string('name', 100);
            $table->text('description');
            $table->unsignedBigInteger('title_id')->nullable();
            $table->string('image_url')->nullable();
            $table->timestamps();
        });

        //Country they come from
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('_id')->unique();
            $table->string('name', 100);
            $table->text('description');
            $table->timestamps();
        });

        //Title of the public official and description
        Schema::create('title', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('_id')->unique();
            $table->string('name', 100);
            $table->text('description');
            $table->timestamps();
        });

        Schema::create('faces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('_id')->unique();
            $table->string('face_token')->nullable();
            $table->unsignedBigInteger('person_id')->nullable();
            $table->unsignedBigInteger('image_id');
            $table->string('faceplus_image_rectangle')->nullable();//plus headpose
            $table->string('faceplus_user_id')->nullable();//user_id that is from a detected face tied to a person
            $table->timestamps();
        });

        //Any image that has faces
        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('_id')->unique();
            $table->string('cloudinary_url');//Url of where it's uploaded from cloudinary
            $table->string('faceplus_image_id');//ImageID from faceplus api
            $table->integer('face_num')->nullable();//number of faces in the image
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
        Schema::dropIfExists('faces');
        Schema::dropIfExists('title');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('people');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Face extends Model
{
    protected $hidden = ['id', 'created_at', 'updated_at'];

    protected $casts = [
        'faceplus_image_rectangle' => 'array',
    ];
}

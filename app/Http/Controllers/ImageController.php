<?php

namespace App\Http\Controllers;

use App\Face;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Ramsey\Uuid\Uuid;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::all();
        return response()->json($images);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        try {
            $this->validate($request, [
                'cloudinary_url' => 'required',
                'faceplus_image_id' => 'required',
                'faces' => 'required',
            ]);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }

        //try catch
        $image = new Image;
        $image->_id = Uuid::uuid4();
        $image->cloudinary_url = $request->input('cloudinary_url');
        $image->faceplus_image_id = $request->input('faceplus_image_id');
        $image->save();

        //Save face details
        $faces = $request->input('faces');
        foreach ($faces as $face) {
            $faceData = new Face();
            $faceData->_id = Uuid::uuid4();
            $faceData->face_token = $face['face_token'];
            $faceData->image_id = $image->id;
            $faceData->faceplus_image_rectangle = $face['face_rectangle'];
            $faceData->save();
        }
        return response()->json($image->load('faces'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show($imageId)
    {
        $image = Image::where('_id', $imageId)->first();
        return response()->json($image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }
}

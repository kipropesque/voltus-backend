<?php

namespace App\Http\Controllers;

use App\Face;
use Exception;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class FaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $face = Face::all();
        return response()->json($face);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        try {
            $this->validate($request, [
                'face_token' => 'required',
                'person_id' => 'required',
                'image_id' => 'required',
                'faceplus_image_rectangle' => 'required|json',
                'faceplus_user_id' => 'required',
            ]);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }

        try {
            $face = new Face;
            $face->_id = Uuid::uuid4();
            $face->face_token = $request->input('face_token');
            $face->person_id = $request->input('person_id'); //Convert to id
            $face->image_id = $request->input('image_id'); //convert this too
            $faceplus_image_rectangle = json_decode($request->input('faceplus_image_rectangle'));
            $face->faceplus_image_rectangle = $faceplus_image_rectangle;
            $face->faceplus_user_id = 1; //$request->input('faceplus_user_id');
            $face->save();
        } catch (Exception $e){
            return response()->json(['error' => $e], 500);
        }

        return response()->json($face);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Face  $face
     * @return \Illuminate\Http\Response
     */
    public function show($faceId)
    {
        $face = Face::where('_id', $faceId)->first();
        return response()->json($face);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Face  $face
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Face $face)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Face  $face
     * @return \Illuminate\Http\Response
     */
    public function destroy(Face $face)
    {
        //
    }
}

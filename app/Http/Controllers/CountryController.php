<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Illuminate\Validation\ValidationException;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        return response()->json($countries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        try {
            $this->validate($request, [
                'name' => 'required',
                'description' => 'required'
            ]);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }

        //try catch
        $country = new country;
        $country->_id = Uuid::uuid4();
        $country->name = $request->input('name');
        $country->description = $request->input('description');
        $country->save();

        return response()->json($country);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country ID $countryID
     * @return \Illuminate\Http\Response
     */
    public function show($countryId)
    {
        $country = Country::where('_id', $countryId)->first();
        return response()->json($country);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        //
    }
}

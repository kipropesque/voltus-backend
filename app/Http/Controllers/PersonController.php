<?php

namespace App\Http\Controllers;

use App\Person;
use \Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Ramsey\Uuid\Uuid;

class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     * List all persons
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $people = Person::all();
        return response()->json($people);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        try {
            $this->validate($request, [
                'name' => 'required',
                'description' => 'required'
            ]);
        } catch (ValidationException $e) {
            return response()->json(['errors' => $e->errors()], 422);
        }

        //try catch
        $person = new Person;
        $person->_id = Uuid::uuid4();
        $person->name = $request->input('name');
        $person->description = $request->input('description');
        $person->save();

        return response()->json($person);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show($personId)
    {
        $person = Person::where('_id', $personId)->with('Face')->first();
        return response()->json($person);
    }

    public function getMultipleUsers(Request $request)
    {
        $user_ids = $request->input('user_ids');
        $person = Person::whereIn('_id', $user_ids)->get();
        return response()->json($person);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Person $person)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person)
    {
        //
    }
}

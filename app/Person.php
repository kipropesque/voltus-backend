<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $hidden = ['id', 'created_at', 'updated_at'];

    /**
     * Get the Face associated with the person.
    */
    public function face()
    {
        return $this->hasOne('App\Face');
    }
}

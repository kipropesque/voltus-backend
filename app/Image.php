<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $hidden = ['id', 'created_at', 'updated_at'];

    public function faces()
    {
        return $this->hasMany('App\Face');
    }
}

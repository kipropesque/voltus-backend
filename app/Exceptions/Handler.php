<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Mail\Message;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $e)
    {
        $environment = env('APP_ENV');
        $debugMode = env('APP_DEBUG');

        $message = $e->getMessage() ? $e->getMessage() : $e->errorInfo;
        $statusCode = $this->isHttpException($e) ? $e->getStatusCode() : 500;

        if ($environment !== 'production' && $debugMode === true) {
            return response()->json([
                'error' => $message,
            ], $statusCode);
        }

        // For validation errors
        if ($e instanceof ValidationException) {
            return parent::render($request, $e);
        }

        if (!$message || $debugMode !== true) {
            $message = 'Internal Server Error';
        }

        return response()->json([
            'error' => 'Internal Server Error',
        ], 500);
    }
}
